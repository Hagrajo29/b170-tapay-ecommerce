const mongoose = require ("mongoose")

const userSchema = new mongoose.Schema ({
	userName: {
		type: String,
		required: [true, "User name is required."]
	},
	email: {
		type: String,
		required: [true, "Email is required."]
	},
	password:{
		type: String,
		required: [true, "Password is required."]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	orderDetails: [
	{
		orderId: {
			type: String,
			required: [true, "Order Id is required."]
		},
		createdOn: {
			type: Date,
			default: new Date ()
		}
	}
	]
})



module.exports = mongoose.model("User", userSchema);
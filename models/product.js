const mongoose = require ("mongoose")

const productSchema = new mongoose.Schema ({
	
	productName : {
		type: String,
		required: [true, "Product name is required."]
	},
	description: {
		type: String,
		required: [true, "Description is required."]
	},
	price:{
		type: Number,
		required: [true, "Price is required."]
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date()
	}/*,
	orderedby:[
	{
		userId: {
			type: String,
			required: [true, "UserId is required."]
		},
		orderId: {
			type: String,
			required: [true, "OrderId is required."]
		}
	}
	]*/

})




module.exports = mongoose.model("Product", productSchema);
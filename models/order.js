const mongoose = require ("mongoose")

const orderSchema = new mongoose.Schema ({
	product: {
		type: mongoose.Schema.Types.ObjectId,
		ref: "Product",
		required: [true, "Product is required."]
	},
	totalAmount : {
		type: Number,
		required: [true, "totalAmount is required."]
	},
	purchasedOn: {
		type: Date,
		default: new Date()
	},
	orderedBy: [
	{
		userId: {
			type: String,
			required: [true, "Used Id is required."]
		}
	}]
})



module.exports = mongoose.model("Order", orderSchema);
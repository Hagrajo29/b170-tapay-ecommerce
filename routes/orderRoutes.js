const express = require("express")
const router = express.Router()

const auth = require("../auth.js")
const orderController = require("../controllers/orderController.js");


router.post("/", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	orderController.addOrder(req.body, userData).then(resultFromController => res.send(resultFromController))
})


router.get("/allOrders", auth.verify, (req, res) => {
	//const userData = auth.decode(req.headers.authorization)
	orderController.getAllOrders().then(resultFromController => res.send(resultFromController))
})



module.exports = router;
const express = require("express")
const router = express.Router()

const auth = require("../auth.js")
const userController = require("../controllers/userController.js")

//user registration
router.post("/register", ( req, res ) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
})


//user login
router.post("/login", ( req, res ) => {
	userController.userLogin(req.body).then(resultFromController => res.send(resultFromController))
})


//user authentication
 router.get("/details", auth.verify, (req, res)=> {
	const userData = auth.decode(req.headers.authorization)
	console.log(userData)
	userController.getProfile( {userId: userData.id} ).then(resultFromController => res.send(resultFromController))
}) 

//set User as Admin

router.put("/:userId/setAsAdmin", auth.verify, (req, res)=> {
	userController.updateUser(req.params, req.body).then(resultFromController => res.send(resultFromController))
})

//create orders
router.post("/checkOut", auth.verify, (req, res) => {
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		orderId: req.body.orderId
	}
	userController.checkOut(data).then(result => res.send(result))
})

//getMyOrder
router.get("/myOrders", auth.verify, (req, res)=> {
	const userData = auth.decode(req.headers.authorization)
	console.log(userData)
	userController.getMyOrders( {userId: userData.id} ).then(resultFromController => res.send(resultFromController))
}) 


module.exports = router;
const express = require("express")
const router = express.Router()

const auth = require("../auth.js")
const productController = require("../controllers/productController.js")


router.post("/", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization)
    productController.addProducts(req.body, userData).then(resultFromController => res.send(resultFromController))
})


router.get("/", (req, res) => {
    productController.getActive().then(resultFromController => res.send(resultFromController))
})


router.get("/:productId",  (req, res) => {
    console.log(req.params.productId);
    productController.getProduct(req.params.productId).then(result => res.send(result))
})


router.put("/:productId/update", auth.verify, (req, res)=> {
    productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController))
})


router.put("/:productId/archive", auth.verify, (req,res)=>{
    productController.archivedProduct(req.params, req.body).then(result =>res.send(result))
})



module.exports = router;
const Order = require("../models/order.js")
const User = require("../models/user.js")
const Product = require("../models/product.js")
const auth = require("../auth.js")
const bcrypt = require("bcrypt")

//adding Order
module.exports.addOrder = (reqBody, userData) => {
    return User.findById(userData.userId).then(result => {
        if (userData.isAdmin === true) {
            return "You can't order."
        } else {
            let newOrder = new Order({
				product: reqBody.product,
				totalAmount: reqBody.totalAmount,
				purchasedOn: reqBody.purchasedOn
			})
            return newOrder.save().then((saved, error) =>{
				if (error){
				console.log(error)
				return false
				}else{
				return newOrder
				}
			})
        }
        
    });    
}


module.exports.getAllOrders = () => {
	return Order.find( {} ).then((result, error) => {
		if (error) {
			return false
		}else{
			return result
		}
	})
}

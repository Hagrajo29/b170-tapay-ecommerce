const User = require("../models/user.js")
const Order = require("../models/order.js")
const auth = require("../auth.js")
const bcrypt = require("bcrypt")

//user registration
module.exports.registerUser = (reqBody) =>{
	let newUser = new User({
		userName: reqBody.userName,
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10)
	})
	return newUser.save().then((saved, error) =>{
		if (error){
			console.log(error)
			return false
		}else{
			return true
		}
	})
}

//user login
module.exports.userLogin = (reqBody) => {
	return User.findOne( { email: reqBody.email } ).then(result => {
		if (result === null) {
			return false
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
			if (isPasswordCorrect) {
				return {access: auth.createAccessToken(result.toObject())}
				
			} else {
				return false
			}
		}
	})
}


//user authentication
module.exports.getProfile = (data) => {
	return User.findById( data.userId ).then(result =>{
		if (result === null) {
			return false
		} else {
			result.password = "";
			return result
		}
	})
}




//set User as Admin
module.exports.updateUser = (reqParams, reqBody) => {
	let updatedUser = {
		isAdmin: reqBody.isAdmin
	} 
		return User.findByIdAndUpdate(reqParams.userId, updatedUser).then((result, error)=> {
			if (error) {
				return false
			}else{
				return true
			}
	})
}



//create order sample
module.exports.checkOut = async (data) => {
	let isUserUpdated = await User.findById(data.userId).then(user => {
		user.orderDetails.push( {orderId: data.orderId} )
		return user.save().then((user, err) => {
			if(err){
				return false
			} else {
				return true
			}
		})
	})
	let isOrderUpdated = await Order.findById(data.orderId).then(order => {
		order.orderedBy.push({userId: data.userId})
		return order.save().then((order, error)=>{
			if(error){
				return false
			}else{
				return true
			}
		})
	})
	if (isUserUpdated && isOrderUpdated){
		return data
	}else{
		return false
	}
}


//getMyOrder
module.exports.getMyOrders = (data) => {
	return User.findById( data.userId )
	.select("orderDetails")
	.exec()
	.then(result =>{
		if (result === null) {
			return false
		} else {
			return result
		}
	})
}



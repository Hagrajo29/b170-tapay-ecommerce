const Product = require("../models/product.js")
const User = require("../models/user.js")
const Order = require("../models/order.js")
const auth = require("../auth.js")
const bcrypt = require("bcrypt")



module.exports.addProducts = (reqBody, userData) => {

    return User.findById(userData.userId).then(result => {
        if (userData.isAdmin === false) {
            return "You are not an admin."
        } else {
            let newProduct = new Product({
                productName: reqBody.productName,
                description: reqBody.description,
                price: reqBody.price
            })
            return newProduct.save().then((result, error) => {
                if(error) {
                    return false
                } else {
                    return result
                }
            })
        }      
    }) 
}


module.exports.getActive = () => {
	return Product.find({isActive:true}).then((result,error)=> {
		if (error) {
			console.log(error)
		}else{
			return result
		}
	})
}


module.exports.getProduct = (reqParams) => {
    return Product.findById(reqParams).then(result => {
        return result
    })
}



module.exports.updateProduct = (reqParams, reqBody) => {
    let updatedProduct = {
        productName: reqBody.productName,
        description: reqBody.description,
        price: reqBody.price
    } 
        return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((result, error)=> {
            if (error) {
                return false
            }else{
                return updatedProduct
            }
    })
}


module.exports.archivedProduct=(reqParams, reqBody)=>{
    let updatedProduct = {
        isActive: reqBody.isActive
    }
        return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((result, error)=>{
            if (error) {
                return false
            }else{
                return updatedProduct
            }
    })
}

